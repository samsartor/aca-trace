static volatile int foo;

int main() {
  foo = 10;
  while (foo > 0) {
    foo--;
  }
}
