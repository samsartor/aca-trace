.PHONY: all clean test

all: aca-trace.so

clean:
	rm -rf tools/obj-ia32
	rm -rf $(PIN_ROOT)
	rm -rf countdown

PIN_ROOT := pin-3.17

$(PIN_ROOT).tar.gz:
	wget https://software.intel.com/sites/landingpage/pintool/downloads/pin-3.17-98314-g0c048d619-gcc-linux.tar.gz -O $(PIN_ROOT).tar.gz

$(PIN_ROOT): $(PIN_ROOT).tar.gz
	mkdir -p $(PIN_ROOT)
	tar -xf $(PIN_ROOT).tar.gz -C $(PIN_ROOT) --strip-components=1

aca-trace.so: $(PIN_ROOT) tools/aca-trace.cpp
	mkdir -p tools/obj-ia32
	cd tools && $(MAKE) PIN_ROOT="../$(PIN_ROOT)" obj-ia32/aca-trace.so
	cp tools/obj-ia32/aca-trace.so .

countdown: countdown.c
	gcc countdown.c -o countdown -g -m32

test: countdown aca-trace.so
	./aca-trace.sh ./countdown
