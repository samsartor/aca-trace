#include <stdio.h>
#include "pin.H"

bool inside_main = false;

void before_main_fn() {
  inside_main = true;
}

void after_main_fn() {
  inside_main = false;
}

void on_image(IMG img, VOID *v) {
  if (IMG_IsMainExecutable(img)) {
    RTN main_fn = RTN_FindByName(img, "_main");

    if (!RTN_Valid(main_fn)) {
      main_fn = RTN_FindByName(img, "main");
    }

    if (!RTN_Valid(main_fn)) {
      printf("Can't find the main function in %s\n", IMG_Name(img).c_str());
      exit(1);
    }

    RTN_Open(main_fn);
    RTN_InsertCall(main_fn, IPOINT_BEFORE, AFUNPTR(before_main_fn), IARG_END);
    RTN_InsertCall(main_fn, IPOINT_AFTER, AFUNPTR(after_main_fn), IARG_END);
    RTN_Close(main_fn);
  }
}

void log_read(void* pc, void* addr) {
  if (inside_main) {
    printf("R %p\n", addr);
  }
}

void log_write(void* pc, void* addr) {
  if (inside_main) {
    printf("W %p\n", addr);
  }
}

void on_instruction(INS ins, void *v) {
  int num_access = INS_MemoryOperandCount(ins);
  for (int i = 0; i < num_access; i++) {
    if (INS_MemoryOperandIsRead(ins, i)) {
      INS_InsertPredicatedCall(
        ins,
        IPOINT_BEFORE,
        (AFUNPTR) log_read,
        IARG_INST_PTR,
        IARG_MEMORYOP_EA,
        i,
        IARG_END);
    }

    if (INS_MemoryOperandIsWritten(ins, i)) {
      INS_InsertPredicatedCall(
        ins,
        IPOINT_BEFORE,
        (AFUNPTR) log_write,
        IARG_INST_PTR,
        IARG_MEMORYOP_EA,
        i,
        IARG_END);
    }
  }
}

int main(int argc, char *argv[]) {
  if (PIN_Init(argc, argv)) return 1;

  PIN_InitSymbols();
  IMG_AddInstrumentFunction(on_image, 0);
  INS_AddInstrumentFunction(on_instruction, 0);
  PIN_StartProgram();

  return 0;
}
